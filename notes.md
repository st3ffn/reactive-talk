Note:
### Threads 
    - complex lifecycle management 
    - easy to produce deadlocks
    - synchronization is painful
    - pretty write only
    - complex debugging and testing
### Future 
    - no real asynchronous computing just when used with executors (get blocking)
    - no listener (a la Netty promise)
    - bad error propagation (only ExecutionException and InterruptedException)
### Callbacks 
    - callback hell (callback inside callback..)
    - often produce Spaghetti code (best to use lambdas)
    - don't scale, 
    - code duplication (lots of callbacks which are pretty alike)
    - no clear channel for errors, 
    - mostly do much more like mapping results, filtering etc., 
    - callbacks often produce memory leaks (when to register when to unregister)
    - how to stop something if listener is not interested anymore?
### Executors
     - bad for testing
### blocking queues 
    - can get out of sync
    - never compact (operations on queue are distributed across class)
