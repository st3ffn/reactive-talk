package samples.s03;

import io.reactivex.annotations.NonNull;
import java.util.List;
import java.util.stream.Collectors;

class StreamSample {

    interface Person {
        int age();
    }

    @NonNull
    public Iterable<Person> peopleBelow18(List<Person> people) {
        return people.stream()
            .filter(person -> person.age() < 18)
            .collect(Collectors.toList());
    }
}
