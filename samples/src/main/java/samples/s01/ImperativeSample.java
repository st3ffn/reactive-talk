package samples.s01;

import io.reactivex.annotations.NonNull;
import java.util.ArrayList;
import java.util.List;

class ImperativeSample {

    interface Person {
        int age();
    }

    @NonNull
    public Iterable<Person> peopleBelow18(List<Person> people) {
        List<Person> filtered = new ArrayList<>();
        for (Person person : people) {
            if (person.age() < 18) {
                filtered.add(person);
            }
        }
        return filtered;
    }
}
