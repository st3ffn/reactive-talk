package samples;

public class Logging {

    public static void log(String callPerformed) {
        System.out.println(Thread.currentThread() + " " + callPerformed);
    }

    public static void log(String callPerformed, Object obj) {
        System.out.println(Thread.currentThread() + " " +  callPerformed + ": " + obj);
    }
}
