package samples.s07;

import io.reactivex.Observable;

public class OperatorsPlayground {
	static Observable<Integer> mapToIntegers(Iterable<String> strings) {
		return Observable.fromIterable(strings)
				.map(Integer::valueOf);
	}

	static Observable<Integer> flatMapToIntegers(Iterable<String> strings) {
		return Observable.fromIterable(strings)
				.flatMap(string -> Observable.just(Integer.valueOf(string)));
	}

	static Observable<Integer> concatMapToIntegers(Iterable<String> strings) {
		return Observable.fromIterable(strings)
				.concatMap(string -> Observable.just(Integer.valueOf(string)));
	}
}
