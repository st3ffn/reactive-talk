package samples.s05;

import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

class RxSample {

    interface Person {
        int age();
    }

    @NonNull
    public Iterable<Person> peopleBelow18_1(List<Person> people) {
        ArrayList<Person> result = new ArrayList	<>();
        Observable.fromIterable(people)
            .filter(person -> person.age() < 18)
            // returns a disposable
            .subscribe(
                // onNext
                person -> result.add(person),
                // onError
                error -> { },
                // onComplete
                () -> {});
        return result;
    }

    @NonNull
    public Iterable<Person> peopleBelow18_2(List<Person> people) {
        return Observable.fromIterable(people)
            .filter(person -> person.age() < 18)
            // Single<List<Person>>
            .toList()
            // leaving the reactive world
            .blockingGet();
    }
}
