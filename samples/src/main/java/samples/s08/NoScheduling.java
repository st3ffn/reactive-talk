package samples.s08;

import static samples.Logging.log;

import io.reactivex.Observable;

class NoScheduling {

    public static void main(String[] args) throws InterruptedException {
        Observable.defer(() -> {
            log("CREATING OBSERVABLE");
            return Observable.just("StEfFEN", "A", "RoNNy");
        })
            .doOnNext(next -> log("onNext", next))
            .map(String::toLowerCase)
            .doOnNext(next -> log("onNext-LOWERED", next))
            .filter(name -> name.length() > 1)
            .doOnNext(next -> log("onNext-FILTERED", next))
            .subscribe(
                next -> log("SUBS-onNext", next),
                error -> log("SUBS-onError", error),
                () -> log("SUBS-onComplete"));
    }
}
