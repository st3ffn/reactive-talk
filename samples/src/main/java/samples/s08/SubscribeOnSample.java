package samples.s08;

import static samples.Logging.log;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

class SubscribeOnSample {

    public static void main(String[] args) throws InterruptedException {
        Observable.defer(() -> {
            log("CREATING OBSERVABLE");
            return Observable.just("StEfFEN", "A", "RoNNy");
        })
            .doOnNext(next -> log("onNext", next))
            .map(String::toLowerCase)
            .doOnNext(next -> log("onNext-LOWERED", next))
            .filter(name -> name.length() > 1)
            .doOnNext(next -> log("onNext-FILTERED", next))
            .subscribeOn(Schedulers.computation())
            .subscribe(
                next -> log("SUBS-onNext", next),
                error -> log("SUBS-onError", error),
                () -> log("SUBS-onComplete"));

        Thread.sleep(1000);
    }
}
