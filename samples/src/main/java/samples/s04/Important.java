package samples.s04;

import static samples.Logging.log;

import io.reactivex.Observable;

public class Important {

	public static void main(String[] args) {
		Observable.just("Having", "fun")
				.doOnSubscribe(ignore -> log("onSubscribe"))
				.doOnNext(s -> log("onNext", s))
				.map(s -> s.length())
				.subscribe(
						s -> log("onNext", s),
						error -> log("onError", error),
						() -> log("onComplete"));
	}
}
