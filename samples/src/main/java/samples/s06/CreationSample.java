package samples.s06;

import java.util.Arrays;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;

import io.reactivex.Observable;

public class CreationSample {

	private interface Response{};
	private Response EMPTY_RESPONSE = new Response() {
	};

	private class PerformRequest implements Callable<Response> {

		@Override
		public Response call() throws Exception {
			return EMPTY_RESPONSE;
		}
	}

	void creation() {
		Observable.just("A", "B");
		Observable.fromArray(new String[]{"A", "B"});
		Observable.fromIterable(Arrays.asList("A", "B"));
		Observable.fromCallable(new PerformRequest());
		Observable.fromFuture(new CompletableFuture<>());

		Observable.defer(() -> Observable.just("A", "B"));
	}
}
