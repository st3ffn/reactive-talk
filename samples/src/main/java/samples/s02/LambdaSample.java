package samples.s02;

import java.util.function.BiFunction;

class LambdaSample {
    public void sample() {
        // (argument-list) -> {body}
        BiFunction<Integer, Integer, Integer> sum = (a, b) -> a+b;
        // result = 7
        int result = sum.apply(2, 5);
    }
}

