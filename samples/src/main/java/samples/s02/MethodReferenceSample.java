package samples.s02;

import java.util.List;

class MethodReferenceSample {

    public void sample(List<String> lines) {
        // using lambda as consumer: (argument-list) -> {body}
        lines.forEach(line -> System.out.println(line));
        // using method reference as consumer Object :: methodName
        lines.forEach(System.out::println);
    }
}
