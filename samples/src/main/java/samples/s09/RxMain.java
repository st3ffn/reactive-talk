package samples.s09;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;

public class RxMain {

	public static class NewsFeedObservable implements ObservableOnSubscribe<String> {

		private final NewsFeed newsFeed;

		public NewsFeedObservable(NewsFeed newsFeed) {
			this.newsFeed = newsFeed;
		}

		@Override
		public void subscribe(ObservableEmitter<String> emitter) throws Exception {
			NewsListener listener = news -> {
				if (!emitter.isDisposed()) {
					System.out.println("onNext");
					emitter.onNext(news);
				}
			};
			emitter.setDisposable(Disposables.fromAction(() -> {
				System.out.println("unregister");
				newsFeed.unregister(listener);
			}));
			System.out.println("register");
			newsFeed.register(listener);
		}
	}

	public static void main(String[] args) {
		NewYorkTimes newYorkTimes = new NewYorkTimes();

		Disposable newsFeedDisposable = Observable.create(new NewsFeedObservable(newYorkTimes))
				.subscribe(news -> System.out.println("News Update: " + news));

		newYorkTimes.publish("Breaking headline 1");
		newYorkTimes.publish("Something felt down");
		newYorkTimes.publish("Somebody did something");
		newYorkTimes.publish("Big boom");

		newsFeedDisposable.dispose();

		newYorkTimes.publish("Secret documents found");
	}
}
