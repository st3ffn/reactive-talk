package samples.s09;

import java.util.HashSet;
import java.util.Set;

class NewYorkTimes implements NewsFeed, Newspaper {

	private Set<NewsListener> listeners = new HashSet<>();

	@Override
	public void register(NewsListener newsListener) {
		listeners.add(newsListener);
	}

	@Override
	public void unregister(NewsListener newsListener) {
		listeners.remove(newsListener);
	}

	@Override
	public void publish(String news) {
		listeners.forEach(newsListener -> newsListener.notify(news));
	}
}
