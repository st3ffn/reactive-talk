package samples.s09;

public class NoRxMain {

	public static void main(String[] args) {
		NewYorkTimes newYorkTimes = new NewYorkTimes();

		NewsListener notifier = news -> System.out.println("News Update: " + news);
		newYorkTimes.register(notifier);

		newYorkTimes.publish("Breaking headline 1");
		newYorkTimes.publish("Something felt down");
		newYorkTimes.publish("Somebody did something");
		newYorkTimes.publish("Big boom");

		newYorkTimes.unregister(notifier);
		newYorkTimes.publish("Secret documents found");
	}
}
