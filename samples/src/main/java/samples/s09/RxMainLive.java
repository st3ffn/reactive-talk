package samples.s09;

class RxMainLive {

    static class NewsFeedObservable {
        //implements ObservableOnSubscribe<String> {
    }

    public static void main(String[] args) {
        NewYorkTimes newYorkTimes = new NewYorkTimes();

        // creation

        newYorkTimes.publish("Breaking headline 1");
        newYorkTimes.publish("Something felt down");
        newYorkTimes.publish("Somebody did something");
        newYorkTimes.publish("Big boom");

        // not interested anymore

        newYorkTimes.publish("Secret documents found");
    }
}
