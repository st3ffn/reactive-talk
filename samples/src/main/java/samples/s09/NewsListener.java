package samples.s09;

/**
 * Listener to get the most recent news (push-based)
 */
interface NewsListener {
	void notify(String news);
}
