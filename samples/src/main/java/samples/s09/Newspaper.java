package samples.s09;

/**
 * A Newspaper can publish news
 */
interface Newspaper {
	void publish(String news);
}
