package samples.s09;

/**
 * News feed where listeners can register to get push based news updates.
 * Listeners can also unregister
 */
interface NewsFeed {
	void register(NewsListener newsListener);
	void unregister(NewsListener newsListener);
}
