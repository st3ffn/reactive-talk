package samples.s08;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.TestScheduler;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ObserveOnSampleTest {

    @Test
    void itsyBitsyTesti() {
        TestScheduler testScheduler = new TestScheduler();
        RxJavaPlugins.setComputationSchedulerHandler(originalScheduler -> testScheduler);

        TestObserver<String> testObserver = ObserveOnSample.filteredNames(
            Observable.just("StEfFEN", "A", "RoNNy"))
            .test();
        // magic
        testScheduler.triggerActions();

        testObserver.assertValues("steffen", "ronny");
        testObserver.assertComplete();
    }
}
