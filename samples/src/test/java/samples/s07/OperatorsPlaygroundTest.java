package samples.s07;

import java.util.Arrays;
import java.util.Collections;

import io.reactivex.observers.TestObserver;
import org.junit.jupiter.api.Test;

import static samples.s07.OperatorsPlayground.mapToIntegers;

/**
 * @author Marcus Herz marcus.herz@kiwigrid.com
 */
class OperatorsPlaygroundTest {

	@Test
	void testEmpty() {
		TestObserver<Integer> testObserver = mapToIntegers(Collections.emptyList())
				// create TestObserver and subscribe to it
				.test();
		testObserver.assertNoValues();
		testObserver.assertComplete();
	}

	@Test
	void testWithValidNumbers() {
		TestObserver<Integer> testObserver = mapToIntegers(Arrays.asList("1", "2", "3"))
				.test();

		testObserver.assertValues(2, 2, 3);
		testObserver.assertComplete();
	}

	@Test
	void testWithInvalidNumber() {
		TestObserver<Integer> testObserver = mapToIntegers(Arrays.asList("1", "BOOOOOOM", "3"))
				.test();

		testObserver.assertValues(1);
		testObserver.assertError(NumberFormatException.class);
	}
}